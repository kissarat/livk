function Contact() {
    $.template(this, 'contact');
    Object.defineProperty(this, 'name', {
        enumerable: false,
        value: ['', '']
    });
}

Contact.prototype.setName = function(first, last) {
    if (first)
        this.name[0] = first;
    if (last)
        this.name[1] = last;
    this.find('div').html(this.name.join(' '));
};

Object.defineProperties(Contact.prototype, {
    uid: {
        get: function() { return parseInt(this.attr('data-uid')); },
        set: function(value) { this.attr('data-uid', value); }
    },
    order: {
        get: function() { return parseInt(this.attr('data-order')); },
        set: function(value) { this.attr('data-order', value); }
    },
    photo: {
        get: function() { return this.find('img').attr('src'); },
        set: function(value) { this.find('img').attr('src', value); }
    },
    first_name: {
        get: function() { return this.find('div').split(' ')[0]; },
        set: function(value) { this.setName(value); }
    },
    last_name: {
        get: function() { return this.find('div').split(' ')[1]; },
        set: function(value) { this.setName(null, value); }
    },
    online: {
        get: function() { return 0 + this.hasClass('online'); },
        set: function(value) {
            if(value && !this.online)
                this.addClass('online');
            else
                this.removeClass('online');
        }
    }
});