const
    fs = require('fs'),
    gui = require('nw.gui');

if (gui.App.argv.length) {
    nick = gui.App.argv[0];
}

new gui.Tray({title:'livk', icon:'img/chat.png'});

jQuery.fn.template = function(self, id) {
    $.extend(self,
        $(document.importNode(
            document.getElementById(id).content, true)))
};

function get_params(href, params) {
//    href = href.slice(1);
    href = href.split('&');
    params = params || {};
    for(var i in href) {
        var param = href[i];
        param = param.split('=');
        params[param[0]] = param[1];
//        params[param[0]] = decodeURIComponent(param[1]);
    }
    return params;
}

function params_to_string(params, keys) {
    var href = [];
    if (keys)
        for(var i in keys)
            href.push(keys[i] + '=' + params[keys[i]]);
    else
        for(var key in params)
            href.push(key + '=' + params[key]);
    return href.join('&');
}

function api(method, params) {
    var url = 'https://api.vk.com/method/';
    if (!params)
        params = {};
    else for(var key in params)
        if ('string' == typeof params[key])
            params[key] = params[key].replace(/[&=%]/g, function(c) {
                return encodeURIComponent(c);
            });
    params['access_token'] = config['access_token'];
    var promise = $.post(url + method, params_to_string(params));
    promise.then(function(data) {
        if (data.error) with (data.error) {
            promise.abort(error_msg);
            console.error(error_msg);
            switch (error_code) {
                case 5:
                    delete config['access_token'];
                    location.reload();
            }
        }
    });
    return promise;
}

var geo;
//const me = config['user_id'];

function rand(max) {
    return Math.round(Math.random()*max);
}

function setMaxTimeout(time, call) {
    return setTimeout(call, rand(time));
}

const
    EVENT_TYPE = 0,
    MESSAGE_ID = 1,
    FLAGS = 2,
    MASK = 2,
    USER_ID = 3,
    FROM_ID = 3,
    TIMESTAMP = 4,
    SUBJECT = 5,
    TEXT = 6;

const mask = {
    unread: 1,
    outbox: 2,
    deleted: 128,
    get: function(number) {
        for(var key in this)
            if(number == this[key])
                return key;
        return false;
    }
};

function sort_friends() {
    $('.friend.unread').sort(function(a, b) {
        return a.hasClass('unread') > b.hasClass('unread');
    });
}

// ************************************* messages.getLongPollServer ----------------------------------------------------
function pull() {
    function a_check() {
        pull.params.wait = rand(120);
        return $.ajax({
            method:'GET',
            dataType: 'json',
            url:'http://' + pull.server + '?' + params_to_string(pull.params),
            success: function(data) {
//                Object.freeze(data);
                switch (data.failed) {
                    case 2:
                        pull.server = null;
                        return setMaxTimeout(3000, pull);
                    default:
                        if ('number' == typeof data.failed)
                            return console.error('Invalid pull server failed code ' + data.failed);
                }
                for(var i in data.updates) {
                    var e = data.updates[i];
                    function boolean(class_name, selector, truth) {
                        if (class_name)
                            $(selector)
                                [truth == e[EVENT_TYPE] ? 'addClass' : 'removeClass']
                                    (class_name);
                    }

                    switch (e[EVENT_TYPE]) {
                        case 1:
                            throw 'ds';
                            break;
                        case 2: // установка флагов сообщения
                        case 3: // сброс флагов сообщения
                            boolean(mask.get(e[MASK]), '[data-mid="' + e[MESSAGE_ID] + '"]', 2);
                            break;
                        case 4: // добавление нового сообщения
                            if (!$dialog)
                                open_dialog(e[FROM_ID]);
                            else
                                addMessage(e[TEXT], e[MESSAGE_ID], e[FROM_ID]);
                            sort_friends();
                            if (mask.outbox & e[FLAGS])
                                break;
                            $('.friend[data-uid="' + e[FROM_ID] + '"].unread > img').addClass('unread');
                            $('#reload').html($('.friend > img.unread').length);
                            break;
                        case 8: // друг $user_id стал онлайн
                        case 9: // друг $user_id стал оффлайн
                            boolean('online', '.friend[data-uid="' + (-e[1]) + '"]', 8);
                            break;
                    }
                }
                console.log(data);
                pull.params.ts = data.ts;
                return setMaxTimeout(1000, a_check);
            }
        })
    }

    if (pull.server)
        a_check();
    else
        api('messages.getLongPollServer').then(function (data) {
            pull.server = data.response.server;
            if (!(delete data.response.server))
                return console.error('Cannot get pull server');
            pull.params = data.response;
            pull.params.act = 'a_check';
            pull.params.mode = 0;
            if (config.ts)
                pull.params.ts = config.ts;
            return a_check();
        });
}

function Vkontakte(nick, win) {
    if (!nick)
        nick = 'default';
    try {
        var config;
        config = fs.readFileSync(nick + '.json');
        config = config.toString('utf-8');
        config = JSON.parse(config);
        $.extend(this, config);
    }
    catch(error) {
        win.alert('Configuration for "' + nick + '" not found');
    }
    this.nick = nick;
    if (!win)
        win = window;
    this.window = win;
    for(var name in Vkontakte.prototype)
        if (Vkontakte.prototype[name] instanceof Function)
            Object.defineProperty(this, name, {
                value: Vkontakte.prototype[name].bind(this),
                enumerable: false,
                writable: false,
                configurable: false
            });
    this.login();
    $.when(
        $.getScript('js/messages.js'),
        $.getScript('js/contacts.js')
    ).then(this.load);
}

Vkontakte.prototype = {
    $: function(selector) {
        return jQuery(selector, this.window.document);
    },

    load: function() {
        if (!config['access_token'])
            return console.error('access_token is not set');
        api('friends.get', {order:'hints', fields:"first_name,last_name,photo,online"}).then(function(data) {
            const $friends = this.$('#friends');
            if (!data || !data.response)
                return err(data);
            data.response.forEach(function(friend, i) {
                var $friend = this.$('<div class="friend"></div>')
                    .attr('data-uid', friend.uid)
                    .attr('data-order', i)
                    .append(this.$('<img/>').attr('src', friend.photo))
                    .append(this.$('<div></div>').html(friend.first_name + ' ' + friend.last_name))
                    .appendTo($friends)
                    .click(open_dialog);
                if (friend.online)
                    $friend.addClass('online');
            });
            pull();
        });

        var $collapse = this.$('#collapse').click(function() {
            if ('&lt;' == $collapse.html()) {
                this.$('.friend > div, #search-friend').css('display', 'none');
                $collapse.html('&gt;');
            }
            else {
                this.$('.friend > div, #search-friend').css('display', '');
                $collapse.html('&lt;');
            }
        });

        this.$('#reload').click(function(){
//        this.classList.add('loading');
            location.reload();
        });

        $.get('https://maps.googleapis.com/maps/api/browserlocation/json?browser=chromium', function(data) {
            if ('ok' == data.status)
                geo = data.location;
        });

        var $friendStyle = this.$('<style type="text/css">.friend {display: none;} .friend.online, .friend.unread {display: table-row;}</style>');
        this.$('#online_only').click(function() {
            if (this.classList.contains('online')) {
                $friendStyle.remove();
                this.classList.remove('online')
            }
            else {
                $friendStyle.appendTo('head');
                this.classList.add('online')
            }
        });
    },

    save: function() {
        var keys = Object.keys(this);
        keys = keys.sort();
        var sorted = {};
        for(var i in keys) {
            var key = keys[i];
            //if (['nick'].indexOf(key) < 0)
                sorted[key] = config[key];
        }
        fs.writeFileSync(this.nick + '.json', JSON.stringify(sorted, null, '\t'));
    },

    login: function() {
        if (!('access_token' in config)) {
            var vk = open('https://oauth.vk.com/authorize?redirect_uri=' +
                encodeURIComponent('/blank.html')
                + '&client_id=4481453&scope=messages,photos,friends,wall&display=page&response_type=token');
            var main = window;
            if(main.vk) vk.onload = function() {
                console.log(this.location.href);
                config = get_params(vk.location.hash.slice(1),
                        localStorage == config ? localStorage : config);
                vk.onunload = function() {
                    main.location.reload();
                };
                vk.close();
            };
            else err('vk is not defined');
        }
    },

    error: function() {
        if ('completed' != this.window.document.readyState)
            alert.apply(this.window, arguments);
        else
            console.error.apply(console, arguments);
    }
};

addEventListener('load', function() {
});

addEventListener('unload', function() {
});

onresize = function() {
    $('.active.dialog').width($('body').width() - $('aside').width() - 10);
};

const KeyCodes = {STRG:17,CTRL:17,CTRLRIGHT:18,CTRLR:18,SHIFT:16,RETURN:13,ENTER:13,BACKSPACE:8,BCKSP:8,ALT:18,ALTR:17,ALTRIGHT:17,SPACE:32,WIN:91,MAC:91,FN:null,UP:38,DOWN:40,LEFT:37,RIGHT:39,ESC:27,DEL:46,F1:112,F2:113,F3:114,F4:115,F5:116,F6:117,F7:118,F8:119,F9:120,F10:121,F11:122,F12:123,backspace:8,tab:9,enter:13,shift:16,ctrl:17,alt:18,pause_break:19,caps_lock:20,escape:27,page_up:33,page_down:34,end:35,home:36,left_arrow:37,up_arrow:38,right_arrow:39,down_arrow:40,insert:45,"delete":46,0:48,1:49,2:50,3:51,4:52,5:53,6:54,7:55,8:56,9:57,a:65,b:66,c:67,d:68,e:69,f:70,g:71,h:72,i:73,j:74,k:75,l:76,m:77,n:78,o:79,p:80,q:81,r:82,s:83,t:84,u:85,v:86,w:87,x:88,y:89,z:90,left_window:91,right_window:92,select_key:93,numpad_0:96,numpad_1:97,numpad_2:98,numpad_3:99,numpad_4:100,numpad_5:101,numpad_6:102,numpad_7:103,numpad_8:104,numpad_9:105,multiply:106,add:107,subtract:109,decimal_point:110,divide:111,f1:112,f2:113,f3:114,f4:115,f5:116,f6:117,f7:118,f8:119,f9:120,f10:121,f11:122,f12:123,num_lock:144,scroll_lock:145,semi_colon:186,equal_sign:187,comma:188,dash:189,period:190,forward_slash:191,grave_accent:192,open_bracket:219,backslash:220,closebracket:221,single_quote:222};
Object.freeze(KeyCodes);
