api.messages = {
    send: function(msg, uid) {
        var params = {
            uid: uid || $('.friend.selected').attr('data-uid'),
            message:msg
        };
        if (geo) {
            params.lat = geo.latitude;
            params.long = geo.longitude;
        }
        return api('messages.send', params);
    }
};

function d(){}

function property(attr, convert) {
    if (!attr)
        attr = 'data-' + name;
    if (!convert)
        convert = parseInt;
    return {
        get: function() { return convert(this.attr(attr)); },
        set: function(value) { this.attr('data-uid', value); }
    };
}

function contentProperty(selector, convert) {
    if (!convert)
        convert = function(value) {return value};
    return {
        get: function() { return convert(this.find(selector).html()); },
        set: function(value) { this.find(selector).html(convert(value)); }
    };
}

function Message(msg) {
    $.template(this, 'message');
    if (msg)
        this.extend(msg);
    else
        this.attr('contenteditable', true);
}

Object.defineProperties(Message.prototype, {
    uid: property('uid'),
    mid: property('mid'),
    body: contentProperty('div:first-child'),
    time: contentProperty('div:last-child'),
    isRead: {
        get: function() {
            return !this.hasClass('unread');
        },
        set: function(value) {
            api('messages.' + (value ? 'markAsRead' : 'markAsNew'),
                {mids: [this.mid]})
                .then(function() {
                    if (value)
                        this.removeClass('unread');
                    else
                        this.addClass('unread');
                });
        }
    }
});

function Dialog() {
    $.template(this, 'dialog');
    var self = this;
    this.editor = new Message()
        .keypress(function(e) {
            if (KeyCodes.ENTER == e.keyCode) {
                e.preventDefault();
                api.messages.send(this.text())
                    .then(function(data) {
                        //self.add(new Message(data));
                        self.editor.empty();
                    })
            }
        });
}

Object.defineProperties(Dialog.prototype, {
    messages: {
        get: function() { return this.find('div'); }
    },
    add: function(message) {
        const self = this;
        api.messages.send(message.body, message.uid)
            .then(function() {
                self.append(message);
            });
    },
    open: function() {
        this.addClass('active');
    },
    close: function() {
        this.removeClass('active');
    }
});

function mark_read() {
    clearTimeout(mark_read.timer);
    var $messages = $dialog.find('div.unread:not(.me)');
    if (0 == $messages.length)
        return;
    mark_read.timer = setTimeout(function() {
        var mids = $messages.map(function(_, e) {
            return parseInt(e.getAttribute('data-mid'));
        });
        mids = mids.toArray();
        api('messages.markAsRead', {mids:mids}).then(function() {
            $messages.removeClass('unread');
        });
    }, 1200);
}

var $dialog;
var $write_message;
function addMessage(msg, mid, uid) {
    var $msg;
    if (msg instanceof jQuery)
        $msg = msg;
    else {
        if ($write_message)
            $write_message.remove();
        $msg = $('<div></div>').html(msg);
    }
    msg = $msg.html();
    msg = msg.replace(/https?[^\s]+/g, function(link) {
        return decodeURI(link);
    });
    $msg.html(msg);
    $msg.attr('data-mid', mid);
//    $msg.css('margin-left', 6 + rand(64) + 'px');
    if (!uid || me == uid)
        $msg.addClass('me');
    $dialog.prepend($msg);
    if (!$dialog.has($write_message))
        $dialog.prepend($write_message);
    return $msg;
}

function open_dialog(uid) {
    var activate = 'object' == typeof uid;
    if (activate) {
        var $selected = $('.friend.selected');
        if (this == $selected[0])
            return;
        uid = this.getAttribute('data-uid');
        $selected.removeClass('selected');
        $selected = $(this)
            .addClass('selected');
        document.title = $selected.find('div').text();
        if ($dialog)
            $dialog.removeClass('active');
    }
    $dialog = $('.dialog[data-uid="' + uid + '"]');
    if (0 == $dialog.length)
        $dialog = $('<article class="dialog"></article>')
            .attr('data-uid', uid)
            .appendTo('body');
    if (activate)
        $dialog.addClass('active');
    resize();

    if (0 == $dialog.children().length)
        return api('messages.getHistory', {uid:uid}).then(function(data) {
            for(var i=data.response.length-1; i>0; i--) {
                var msg = data.response[i];
                var $msg = addMessage(msg.body, msg.mid, msg.uid);
                if (!msg.read_state)
                    $msg.addClass('unread');
            }
            $write_message = $('<div contenteditable="true"></div>')
                .prependTo($dialog)
                .keypress(function(e) {
//                    var $write_message = $(this);
                    if (KeyCodes.ENTER == e.keyCode) {
                        e.preventDefault();
                        api.messages.send($write_message.text())
                            .then(function(data) {
                                $write_message.remove();
                                var $new_message = $write_message.clone(true);
                                addMessage($new_message, data.response)
                                    .unbind('keypress')
                                    .removeAttr('contenteditable');
                                $write_message.empty();
                                $dialog.prepend($write_message);
                                $write_message.focus();
                            })
                    }
                });
            mark_read();
        });
    else
        mark_read();
}

$(document).ready(function() {

});